import { CALL_API, CHAIN_API } from 'middleware/api'

export const LOADED_CLIENTS = Symbol('LOADED_CLIENTS')

export function loadClients() {
  return {
    [CALL_API]: {
      method: 'get',
      path: '/clients',
      successType: LOADED_CLIENTS
    }
  }
}

export const ADD_CLIENTS = Symbol('ADD_CLIENTS')


export function addClients(client) {
  return {
    [CALL_API]: {
      method: 'post',
      path: '/clients',
      successType: ADD_CLIENTS,
      body:client
    }
  }
}

export const UPDATED_CLIENTS = Symbol('UPDATED_CLIENTS')
export function updateClient({id}) {
  return {
    [CALL_API]: {
      method: 'put',
      path: `/clients/${id}`,
      successType: UPDATED_CLIENTS
    }
  }
}

export const GET_CLIENT = Symbol('GET_CLIENT')
export function getClient(id) {
  return {
    [CALL_API]: {
      method: 'get',
      path: `/clients/${id}`,
      successType: GET_CLIENT
    }
  }
}

export const DELETE_CLIENT = Symbol('DELETE_CLIENT')
export function deleteClient(id) {
  return {
    [CALL_API]: {
      method: 'delete',
      path: `/clients/${id}`,
      successType: DELETE_CLIENT
    }
  }
}