import React, { Component } from 'react';
import ReactDOM, {Input} from 'react-dom';
import { connect } from 'react-redux';
import tv4 from 'tv4';
import {loadClients,addClients,deleteClient,getClient} from 'actions/clients'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table ,UncontrolledTooltip} from 'reactstrap';
import Pagination from 'components/Pagination'
import Moment from 'moment';
import Select from 'react-select';

/**
  tv4 custom message
*/

// tv4.setErrorReporter(function (error, data, schema) {
//   if(schema.errorMessage && schema.errorMessage[error.code]){
//       return schema.errorMessage[error.code];
//   }
//   return;
// });
class Clients extends Component {
    constructor(props){
        super(props);
        this.state = {
            modal: false ,
            backdrop: true,
            modalLabel: "Add Client",
            options: [
				{ value: 0, label: 'First contact with client through cold call' },
				{ value: 1, label: 'Product Demonstration' },
				{ value: 2, label: 'Signing of service contract' },
                { value: 3, label: 'Payment of service' },
                { value: 4, label: 'Tech Delivery/Setup' },
				{ value: 5, label: 'Training' }
			],
			matchPos: 'any',
			matchValue: true,
            matchLabel: true,
            disabled:true,
			value: null,
			multi: false
        };
        this.toggle = this.toggle.bind(this);        
    }
    
    static fetchData({ store }) {
        return store.dispatch(loadClients())
    }
    componentDidMount() {
        this.props.loadClients()
    }

    filter(stageCode){
        return this.state.options[stageCode].label || '--';
    }

    formatDate(date){
        return Moment(date).format('DD-MM-YYYY , h:mm:ss a');
    }

    toggle(data) {
        let isView = false;
        if(!data){
            
        }else{
            isView = true;
        }
        this.setState({
            modal: !this.state.modal,
            data: this.state.data,
            isView:isView
        });
    }

    handleEdit(item, e) {
        e.preventDefault();
        this.props.getClient(item.get('id'));
    }  

    handleDelete(item, index, e) {
        e.preventDefault();
        console.log("item.id",index);
        if(confirm("Are you sure you want to delete?")){
          //Call API to delete client
          console.log("item.id",item);
        }
    }  

    onChange(value) {
		// this.setState({ value });
		console.log('Numeric Select value changed to', value);
	}


    
    render() {
        console.log("action client",this.props.clientDetail)
        return (
            <div className="card">
                <div className="header">
                    <h3>List Client</h3>
                </div>
                <div className="content">
                    <Button id="addButton" color="btn btn-primary btn-info btn-fill" onClick={this.toggle}> 
                        <i className="pe-7s-add-user"></i>
                        Add Client
                    </Button>
                    <UncontrolledTooltip placement="right" target="addButton">
                        Add Client 
                    </UncontrolledTooltip> 
                <Table striped>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Client Name</th>
                        <th className="minW190">Stage</th>
                        <th>Created Date</th>
                        <th>Updated Date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                        this.props.clients.map((q,index)=> {
                            let id = q.get('id')
                            return (
                            <tr key={id}>
                                <th scope="row">{index +1}</th>
                                <td><a className="action" href={"/clients/" + q.get('id')}> {q.get('clientName')} </a>
                                </td>
                                <td>
                                    {/* <Select
                                        matchPos={this.state.matchPos}
                                        matchProp='any'
                                        multi={this.state.multi}
                                        onChange={this.onChange}
                                        options={this.state.options}
                                        simpleValue
                                        value={q.get('stage')}
                                    /> */}
                                    {this.filter(q.get('stage'))}
                                </td>
                                <td>{this.formatDate(q.get('createdAt'))}</td>
                                <td>{this.formatDate(q.get('updatedAt'))}</td>
                                <td className="minW120">
                                    <a className="action" href="#" onClick={this.handleEdit.bind(this, q)}> <i className="fa fa-pencil-square-o  fa-2x edit"></i> </a>
                                    <a className="action" href="#" onClick={this.handleDelete.bind(this, q, index)}> <i className="fa fa-trash fa-2x remove"></i> </a>
                                </td>
                            </tr>
                            )
                        })
                        }
                    
                    </tbody>
                </Table>
                {/* <Pagination /> */}
                </div>
                    
                
            </div>
          );
    }
  }
  
  function mapStateToProps(state) {
    return { clients: state.clients,
             clientDetail : state.clientDetail }
  }
  
  export default connect(mapStateToProps, { loadClients ,getClient })(Clients)