import { combineReducers } from 'redux'
import clients from 'reducers/clients'

const rootReducer = combineReducers({
  clients
})

export default rootReducer
