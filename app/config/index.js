let config = {
  API_BASE_URL: process.env.API_BASE_URL,
  ROUTENAMES: [
    { path: "/dashboard", name: "Dashboard"},
    { path: "/clients", name: "Client" }
  ]
}

export default config;
