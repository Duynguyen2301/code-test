import * as ActionType from 'actions/clients'
import Immutable from 'immutable'

let defaultState = Immutable.fromJS([])
function clientsReducer (state = defaultState, action) {
  switch(action.type) {
    case ActionType.LOADED_CLIENTS:
      return Immutable.fromJS(action.response)
      break
    default:
      return state
  }
}

export default clientsReducer
