import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute } from 'react-router'
import configureStore from 'store/configureStore'
import Client from 'containers/client/clientForm'

export default function(history) {
  return (
    <Router history={history}>
      <Route path="/" component={Client}>
        <Route path="clients" component={Client} />
        <Route path="clients/:id" component={Client} />
        <IndexRoute component={Client} />
      </Route>
    </Router>
  )
}
